/// <reference path="_all.ts" />

// Statically typed classes

module baristaApp {
    export class CreateDrink {
        constructor(
            public firstName: string,
            public lastName: string,
            public avatar: string,
            public description: string)  {
        }
    }

    export class Drink {
        constructor(
            public name: string,
            public avatar: string,
            public description: string,
            public notes: Note[],
            public items: Items[],
            public ingredients: Ingredients[])  {
        }

        static fromCreate(drink: CreateDrink): Drink {
            return new Drink(
                drink.firstName + ' ' + drink.lastName,
                drink.avatar,
                drink.description,
                []);
        }
    }

    export class Note {
        constructor(
            public title: string,
            public date: Date) {
        }
    }


    export class Ingredients {
        constructor(
            public coffee: string) {
        }
    }

    export class Invent {
        constructor(
            public name: string,
            public avatar: string,
            public quantity: string
        ) {

        }
    }
}