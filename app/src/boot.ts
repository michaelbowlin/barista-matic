/// <reference path="_all.ts" />

// This is where we register services and controllers within the module

module baristaApp {
    angular.module('baristaApp', ['ngMaterial', 'ngMdIcons'])
        .service('drinkService', DrinkService)
        .service('inventService', InventService)
        .controller('mainController', MainController)
        .config(($mdIconProvider: angular.material.IIconProvider,
                $mdThemingProvider: angular.material.IThemingProvider) => {

            $mdIconProvider
                .icon('menu', './assets/svg/menu.svg',24)
                .defaultIconSet('./assets/svg/avatars.svg', 128);

            $mdThemingProvider.theme('default')
                .primaryPalette('blue')
                .accentPalette('red')

        });
    ;
}