/// <reference path="../_all.ts" />

// using the q service to simulate loading drinks from a server

module baristaApp {

    export interface IDrinkService {
        loadAllDrinks(): ng.IPromise<Drink[]>;
        selectedDrink: Drink;
    }

    export class DrinkService implements IDrinkService {
        static $inject = ['$q'];

        constructor(private $q:ng.IQService) {
        }

        selectedDrink:Drink = null;

        loadAllDrinks():ng.IPromise<Drink[]> {
            return this.$q.when(this.drinks);
        }

        // Drink Model
        private drinks:Drinks[] = [
            {
                drinkName: 'Coffee',
                avatar: 'img-coffee.png',
                description: 'Coffee -Our dark roast coffees are the result of Baristamatic\'s long history of roasting and blending expertise, creativity and passion. Varying roast intensities bring out the distinct flavors in each bean variety, so you can find the flavor profile that best suits your palate.',
                items: [
                    {
                        name: 'Coffee',
                        quantity: 3
                    }, {
                        name:'Sugar',
                        quantity: 1
                    }, {
                        name: 'Cream',
                        quantity: 1
                    }]

            },
            {
                drinkName: 'Decaf Coffee',
                avatar: 'img-decafcoffee.png',
                description: 'Decaf Coffee - Our dark roast coffees are the result of Baristamatic\'s long history of roasting and blending expertise, creativity and passion. Varying roast intensities bring out the distinct flavors in each bean variety, so you can find the flavor profile that best suits your palate.',
                items: [
                    {
                        name: 'Decafe Coffee',
                        quantity: 3
                    }, {
                        name:'Sugar',
                        quantity: 1
                    }, {
                        name: 'Cream',
                        quantity: 1
                    }]

            },
            {
                drinkName: 'Caffe Latte',
                avatar: 'img-caffelatte.png',
                description: 'Caffe Latte - Our dark roast coffees are the result of Baristamatic\'s long history of roasting and blending expertise, creativity and passion. Varying roast intensities bring out the distinct flavors in each bean variety, so you can find the flavor profile that best suits your palate.',
                items: [
                    {
                        name: 'Espresso',
                        quantity: 2
                    }, {
                        name:'Steamed Milk',
                        quantity: 2
                    }]

            },
            {
                drinkName: 'Caffe Americano',
                avatar: 'img-caffeamericano.png',
                description: 'Caffe Americano - Our dark roast coffees are the result of Baristamatic\'s long history of roasting and blending expertise, creativity and passion. Varying roast intensities bring out the distinct flavors in each bean variety, so you can find the flavor profile that best suits your palate.',
                items: [
                    {
                        name: 'Espresso',
                        quantity: 3
                    }]

            },
            {
                drinkName: 'Caffe Mocha',
                avatar: 'img-caffemocha.png',
                description: 'Caffe Mocha - Our dark roast coffees are the result of Baristamatic\'s long history of roasting and blending expertise, creativity and passion. Varying roast intensities bring out the distinct flavors in each bean variety, so you can find the flavor profile that best suits your palate.',
                items: [
                    {
                        name: 'Espresso',
                        quantity: 1
                    },
                    {
                        name: 'Cocoa',
                        quantity: 1
                    },
                    {
                        name: 'Steamed Milk',
                        quantity: 1
                    },
                    {
                        name: 'Whipped Cream',
                        quantity: 1
                    }]

            },
            {
                drinkName: 'Cappuccino',
                avatar: 'img-cappuccino.png',
                description: 'Cappuccino - Our dark roast coffees are the result of Baristamatic\'s long history of roasting and blending expertise, creativity and passion. Varying roast intensities bring out the distinct flavors in each bean variety, so you can find the flavor profile that best suits your palate.',
                items: [
                    {
                        name: 'Espresso',
                        quantity: 2
                    },
                    {
                        name: 'Steamed Milk',
                        quantity: 1
                    },
                    {
                        name: 'Foamed Milk',
                        quantity: 1
                    }]

            }
        ];
    }
}