/// <reference path="../_all.ts" />

// using the q service to simulate loading invents from a server

module baristaApp {

    export interface IInventService {
        loadAllInvents(): ng.IPromise<Invent[]>;
        selectedInvent: Invent;
    }

    export class InventService implements IInventService {
        static $inject = ['$q'];

        constructor(private $q: ng.IQService) {
        }

        selectedInvent: Invent = null;

        loadAllInvents() : ng.IPromise<Invent[]> {
            return this.$q.when(this.invents);
        }

        // Invent Model
        private invents: Invents[] = [
            {
                name: 'Coffee',
                unitCost: .75,
                quantity: 10
            },
            {
                name: 'Decafe Coffee',
                unitCost: .75,
                quantity: 10
            },
            {
                name: 'Sugar',
                unitCost: .75,
                quantity: 10
            },
            {
                name: 'Cream',
                unitCost: .25,
                quantity: 10
            },
            {
                name: 'Steamed Milk',
                unitCost: .35,
                quantity: 10
            },
            {
                name: 'Foamed Milk',
                unitCost: .35,
                quantity: 10
            },
            {
                name: 'Espresso',
                unitCost: 1.10,
                quantity: 10
            },
            {
                name: 'Cocoa',
                unitCost: .90,
                quantity: 10
            },
            {
                name: 'Whipped Cream',
                unitCost: 1,
                quantity: 10
            }
        ];
    }
}