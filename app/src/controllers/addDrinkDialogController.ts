/// <reference path="../_all.ts" />

module baristaApp {

    export class AddDrinkDialogController {
        static $inject = ['$mdDialog'];

        constructor(private $mdDialog) {}

        drink: CreateDrink;

        avatars = [
            'img-coffee.png','img-decafcoffee.png','img-cappuccino.png'
        ];

        cancel(): void {
            this.$mdDialog.cancel();
        }

        save(): void {
            //this.$mdDialog.hide(this.drink);
            this.$mdDialog.hide(new Drink("placeholder", "", "", [], []));
        }
    }
}