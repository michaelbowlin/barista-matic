/// <reference path="../_all.ts" />

// TimeStamp 2:29

module baristaApp {
    export class MainController {
        // inject is for minification of files
        static $inject = [
            'drinkService',
            'inventService',
            '$mdSidenav',
            '$mdToast',
            '$mdDialog',
            '$mdMedia',
            '$mdBottomSheet'];

        constructor(private drinkService:IDrinkService,
                    private inventService:IInventService,
                    private $mdSidenav:angular.material.ISidenavService,
                    private $mdToast:angular.material.IToastService,
                    private $mdDialog:angular.material.IDialogService,
                    private $mdMedia:angular.material.IMedia,
                    private $mdBottomSheet:angular.material.IBottomSheetService) {

            var self = this,
                ingredients = [],
                ingredientsNames = [],
                ingredientsQuantity = [];

            // 1) INGREDIENTS FUNCTION EXPRESSION
            var inventory = function (drinkIng, drinkQuantity) {

                self.inventService
                    .loadAllInvents()
                    .then((invents:Invent[]) => {
                        self.invents = invents;
                        //console.log(self.invents);
                        ingredients.push(self.invents);


                        /*
                         *
                         *           //TODO: Finish pairing of Drink service and Inventory Service
                         *           IN PROGRESS / standard js looping
                         *           *** A version of this method is also available
                         *           in LoDash here:
                         *                          http://codepen.io/michaelbowlin/pen/LkmKZp?editors=0001
                         *
                         *
                         * /
                         */
                        //console.log(invents.length);
                        var numIng = invents.length;
                        for (var z in self.invents) {
                            // push names into array
                            ingredientsNames.push(invents[z].name);
                            // push quantity into array
                            ingredientsQuantity.push(invents[z].quantity);
                        }

                        // Find index position of drinkIng
                        var drinkIngIndex = ingredientsNames.indexOf(drinkIng);

                        // does drinkIng show up in ingredientsNames array?
                        if (drinkIngIndex !== -1) {
                            var inventoryAmount = ingredientsQuantity[drinkIngIndex];

                            if (drinkQuantity <= inventoryAmount) {
                                console.log('Quantity of ' + drinkIng + ' is ' + drinkQuantity +
                                    ' Quantity of inventory item is ' + inventoryAmount + '. *** Good to buy the drink' )
                            } else {
                                console.log('Quantity of ' + drinkIng + ' is ' + drinkQuantity +
                                    ' Quantity of inventory item is ' + inventoryAmount + '. *** Need to restock');
                            }

                        } else {
                            console.log(drinkIng + 'NOT there')
                        }
                    });

            };

            // 1) LOAD DRINKS
            this.drinkService
                .loadAllDrinks()
                .then((drinks:Drink[]) => {
                    self.drinks = drinks;
                    self.selected = drinks[0];

                    /*
                    *
                    *           //TODO: Finish pairing of Drink service and Inventory Service
                    *           IN PROGRESS / standard js looping
                    *           *** A version of this method is also available
                    *           in LoDash here:
                    *                          http://codepen.io/michaelbowlin/pen/LkmKZp?editors=0001
                    *
                    *
                    * /
                     */
                    // 2) DRINKS FUNCTION EXPRESSION
                    var drinksFE = function(drinkName, drinkItemsNameArray, drinkItemsQuantityArray) {
                        console.log(drinkName, drinkItemsNameArray, drinkItemsQuantityArray);

                        // MACHINE TO MAKE THE DRINK

                        // which has a list of these ingredients: drinkItemsNameArray
                        // loop through drinkItemsNameArray
                        for(var q in drinkItemsNameArray) {
                            console.log(drinkItemsNameArray[q]);
                            var dIName = drinkItemsNameArray[q];
                        }

                        // loop through drinkItemsQuantityArray
                        for(var q in drinkItemsQuantityArray) {
                            console.log(drinkItemsQuantityArray[q]);
                            var dIQuantity = drinkItemsQuantityArray[q];
                        }

                        // Join two arrays into object
                        var toObject = function(drinkItemsNameArray, drinkItemsQuantityArray) {
                            var result = {};
                            for (var i = 0; i < drinkItemsNameArray.length; i++)
                                result[drinkItemsNameArray[i]] = drinkItemsQuantityArray[i];
                            return result;
                        }
                        var drinkObject = [];

                        // create loop that runs equal to array(s) length(s)
                        var aLength = drinkItemsQuantityArray.length;
                        for(var i = 0; i < aLength; i++) {

                        }

                        drinkObject.push({dIName, dIQuantity});

                        console.log(drinkObject);

                        // Check against inventory
                        inventory(drinkName,'20');
                        toObject();
                    };

                    // static parameters
                    var drinkItemsNameAr = ['sugar','cream','grounds'];
                    var drinkItemsQuantityAr = [3, 2, 4];

                    drinksFE('Americano', drinkItemsNameAr, drinkItemsQuantityAr);
                });
        }

        searchText:string = ''; // for autocomplete
        drinks:Drinks[] = [];
        invents:Invents[] = [];
        selected:Drink = null;
        restockDrink:Drink = null;
        restockInventory:Invent = null;
        selectedInvent:Invent = null;
        message:string = "Hello from our controller";

        // Method - Toggle SideNav
        toggleSideNav():void {
            this.$mdSidenav('left').toggle();
        }

        // Method - Select Drink
        selectDrink(drink:Drink):void {
            this.selected = drink;
            this.restockDrink = drink;

            // Close menu if needed
            var sidenav = this.$mdSidenav('left');
            if (sidenav.isOpen()) {
                sidenav.close();
            }
        }

        // Method - Add Drink
        addDrink($event) {
            var self = this;
            var useFullScreen = (this.$mdMedia('sm') || this.$mdMedia('xs'));

            this.$mdDialog.show({
                templateUrl: './dist/views/newDrinkDialog.html',
                parent: angular.element(document.body),
                targetEvent: $event,
                controller: AddDrinkDialogController,
                controllerAs: 'ctrl',
                clickOutsideToClose: true,
                fullscreen: useFullScreen
            }).then((drink:CreateDrink) => {
                var newDrink:Drink = Drink.fromCreate(drink);
                self.drinks.push(newDrink);
                self.selectDrink(newDrink);
                self.openToast("Drink added");
            }, () => {
                console.log('You cancelled the dialog.');
            });
        }

        // Method - Make Drink


    }
}