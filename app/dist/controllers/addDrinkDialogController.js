/// <reference path="../_all.ts" />
var baristaApp;
(function (baristaApp) {
    var AddDrinkDialogController = (function () {
        function AddDrinkDialogController($mdDialog) {
            this.$mdDialog = $mdDialog;
            this.avatars = [
                'img-coffee.png', 'img-decafcoffee.png', 'img-cappuccino.png'
            ];
        }
        AddDrinkDialogController.prototype.cancel = function () {
            this.$mdDialog.cancel();
        };
        AddDrinkDialogController.prototype.save = function () {
            //this.$mdDialog.hide(this.drink);
            this.$mdDialog.hide(new baristaApp.Drink("placeholder", "", "", [], []));
        };
        AddDrinkDialogController.$inject = ['$mdDialog'];
        return AddDrinkDialogController;
    }());
    baristaApp.AddDrinkDialogController = AddDrinkDialogController;
})(baristaApp || (baristaApp = {}));
//# sourceMappingURL=addDrinkDialogController.js.map