/// <reference path="../_all.ts" />
// using the q service to simulate loading drinks from a server
var baristaApp;
(function (baristaApp) {
    var DrinkService = (function () {
        function DrinkService($q) {
            this.$q = $q;
            this.selectedDrink = null;
            // Drink Model
            this.drinks = [
                {
                    drinkName: 'Coffee',
                    avatar: 'img-coffee.png',
                    description: 'Coffee -Our dark roast coffees are the result of Baristamatic\'s long history of roasting and blending expertise, creativity and passion. Varying roast intensities bring out the distinct flavors in each bean variety, so you can find the flavor profile that best suits your palate.',
                    items: [
                        {
                            name: 'Coffee',
                            quantity: 3
                        }, {
                            name: 'Sugar',
                            quantity: 1
                        }, {
                            name: 'Cream',
                            quantity: 1
                        }]
                },
                {
                    drinkName: 'Decaf Coffee',
                    avatar: 'img-decafcoffee.png',
                    description: 'Decaf Coffee - Our dark roast coffees are the result of Baristamatic\'s long history of roasting and blending expertise, creativity and passion. Varying roast intensities bring out the distinct flavors in each bean variety, so you can find the flavor profile that best suits your palate.',
                    items: [
                        {
                            name: 'Decafe Coffee',
                            quantity: 3
                        }, {
                            name: 'Sugar',
                            quantity: 1
                        }, {
                            name: 'Cream',
                            quantity: 1
                        }]
                },
                {
                    drinkName: 'Caffe Latte',
                    avatar: 'img-caffelatte.png',
                    description: 'Caffe Latte - Our dark roast coffees are the result of Baristamatic\'s long history of roasting and blending expertise, creativity and passion. Varying roast intensities bring out the distinct flavors in each bean variety, so you can find the flavor profile that best suits your palate.',
                    items: [
                        {
                            name: 'Espresso',
                            quantity: 2
                        }, {
                            name: 'Steamed Milk',
                            quantity: 2
                        }]
                },
                {
                    drinkName: 'Caffe Americano',
                    avatar: 'img-caffeamericano.png',
                    description: 'Caffe Americano - Our dark roast coffees are the result of Baristamatic\'s long history of roasting and blending expertise, creativity and passion. Varying roast intensities bring out the distinct flavors in each bean variety, so you can find the flavor profile that best suits your palate.',
                    items: [
                        {
                            name: 'Espresso',
                            quantity: 3
                        }]
                },
                {
                    drinkName: 'Caffe Mocha',
                    avatar: 'img-caffemocha.png',
                    description: 'Caffe Mocha - Our dark roast coffees are the result of Baristamatic\'s long history of roasting and blending expertise, creativity and passion. Varying roast intensities bring out the distinct flavors in each bean variety, so you can find the flavor profile that best suits your palate.',
                    items: [
                        {
                            name: 'Espresso',
                            quantity: 1
                        },
                        {
                            name: 'Cocoa',
                            quantity: 1
                        },
                        {
                            name: 'Steamed Milk',
                            quantity: 1
                        },
                        {
                            name: 'Whipped Cream',
                            quantity: 1
                        }]
                },
                {
                    drinkName: 'Cappuccino',
                    avatar: 'img-cappuccino.png',
                    description: 'Cappuccino - Our dark roast coffees are the result of Baristamatic\'s long history of roasting and blending expertise, creativity and passion. Varying roast intensities bring out the distinct flavors in each bean variety, so you can find the flavor profile that best suits your palate.',
                    items: [
                        {
                            name: 'Espresso',
                            quantity: 2
                        },
                        {
                            name: 'Steamed Milk',
                            quantity: 1
                        },
                        {
                            name: 'Foamed Milk',
                            quantity: 1
                        }]
                }
            ];
        }
        DrinkService.prototype.loadAllDrinks = function () {
            return this.$q.when(this.drinks);
        };
        DrinkService.$inject = ['$q'];
        return DrinkService;
    }());
    baristaApp.DrinkService = DrinkService;
})(baristaApp || (baristaApp = {}));
//# sourceMappingURL=drinkService.js.map