/// <reference path="../_all.ts" />
// using the q service to simulate loading invents from a server
var baristaApp;
(function (baristaApp) {
    var InventService = (function () {
        function InventService($q) {
            this.$q = $q;
            this.selectedInvent = null;
            // Invent Model
            this.invents = [
                {
                    name: 'Coffee',
                    unitCost: .75,
                    quantity: 10
                },
                {
                    name: 'Decafe Coffee',
                    unitCost: .75,
                    quantity: 10
                },
                {
                    name: 'Sugar',
                    unitCost: .75,
                    quantity: 10
                },
                {
                    name: 'Cream',
                    unitCost: .25,
                    quantity: 10
                },
                {
                    name: 'Steamed Milk',
                    unitCost: .35,
                    quantity: 10
                },
                {
                    name: 'Foamed Milk',
                    unitCost: .35,
                    quantity: 10
                },
                {
                    name: 'Espresso',
                    unitCost: 1.10,
                    quantity: 10
                },
                {
                    name: 'Cocoa',
                    unitCost: .90,
                    quantity: 10
                },
                {
                    name: 'Whipped Cream',
                    unitCost: 1,
                    quantity: 10
                }
            ];
        }
        InventService.prototype.loadAllInvents = function () {
            return this.$q.when(this.invents);
        };
        InventService.$inject = ['$q'];
        return InventService;
    }());
    baristaApp.InventService = InventService;
})(baristaApp || (baristaApp = {}));
//# sourceMappingURL=inventService.js.map