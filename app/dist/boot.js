/// <reference path="_all.ts" />
// This is where we register services and controllers within the module
var baristaApp;
(function (baristaApp) {
    angular.module('baristaApp', ['ngMaterial', 'ngMdIcons'])
        .service('drinkService', baristaApp.DrinkService)
        .service('inventService', baristaApp.InventService)
        .controller('mainController', baristaApp.MainController)
        .config(function ($mdIconProvider, $mdThemingProvider) {
        $mdIconProvider
            .icon('menu', './assets/svg/menu.svg', 24)
            .defaultIconSet('./assets/svg/avatars.svg', 128);
        $mdThemingProvider.theme('default')
            .primaryPalette('blue')
            .accentPalette('red');
    });
    ;
})(baristaApp || (baristaApp = {}));
//# sourceMappingURL=boot.js.map