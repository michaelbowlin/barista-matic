/// <reference path="_all.ts" />
// Statically typed classes
var baristaApp;
(function (baristaApp) {
    var CreateDrink = (function () {
        function CreateDrink(firstName, lastName, avatar, description) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.avatar = avatar;
            this.description = description;
        }
        return CreateDrink;
    }());
    baristaApp.CreateDrink = CreateDrink;
    var Drink = (function () {
        function Drink(name, avatar, description, notes, items, ingredients) {
            this.name = name;
            this.avatar = avatar;
            this.description = description;
            this.notes = notes;
            this.items = items;
            this.ingredients = ingredients;
        }
        Drink.fromCreate = function (drink) {
            return new Drink(drink.firstName + ' ' + drink.lastName, drink.avatar, drink.description, []);
        };
        return Drink;
    }());
    baristaApp.Drink = Drink;
    var Note = (function () {
        function Note(title, date) {
            this.title = title;
            this.date = date;
        }
        return Note;
    }());
    baristaApp.Note = Note;
    var Ingredients = (function () {
        function Ingredients(coffee) {
            this.coffee = coffee;
        }
        return Ingredients;
    }());
    baristaApp.Ingredients = Ingredients;
    var Invent = (function () {
        function Invent(name, avatar, quantity) {
            this.name = name;
            this.avatar = avatar;
            this.quantity = quantity;
        }
        return Invent;
    }());
    baristaApp.Invent = Invent;
})(baristaApp || (baristaApp = {}));
//# sourceMappingURL=models.js.map