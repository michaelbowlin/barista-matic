# Barista-Matic Angular Application

Barista-Matic is an Angular application that is a simulator of an automatic coffee dispensing machine. The machine maintains an inventory of drink ingredients, and is able to dispense a fixed set of possible drinks by combining these ingredients in different amounts. The cost of a drink is determined by its component ingredients.

 - [Setup Angular Material](#setup)
 - [Documentation](#documentation)
 - [Technologies used](#technologies)
 - [Typescript specifics](#typescript)
 - [Versioning](#versioning)


## Setup Angular Material

Install `npm`, Node.js's package manager. If you do not have Node and npm installed, the directions for how to do that can be found at [Node's official site](http://nodejs.org/download/).

Next install `bower`

```
npm install bower -g
```

Then install Angular Material with Bower. This will install the following in the bower_components folder:
- angular
- angular-animate
- angular-aria
- angular-material
- angular-messages

```
bower install angular-material
```

Create a bower.json file

```
bower install angular-material --save
bower init
```

Set up Typescript

```
npm install typescript -g
```

Get Typescript definition files, TSD. You can query and get definition files from this GitHub repo:  [TSD Definition Files](https://github.com/DefinitelyTyped/tsd)

```
npm install tsd -g
```

Get the specific definition files for Angular and Angular Material

```
tsd query angular
tsd query angular-material
tsd install angular angular-material
```

Open a new terminal window and point your directory to app/src

```
cd app/src
```

In terminal set up a tsc watch for when you update your files.  This will watch the files and transpile upon save.

```
tsc -watch
```

In windows:

```
tsc.cmd -w
```

To serve the application you will need another terminal window open and run live-server and live-reload

```
npm install live-server -g
npm install live-reload -g
cd app
live-server
```

Install Angular Material Icons

```
bower install angular-material-icons --save
```

## Documentation

Coming Soon...

## Technologies used

Angular Material provides a set of reusable, well-tested, and accessible UI components based on Google's Material Design specification. So far, this includes:

- Angular JS (1.4.x)
- Angular Animate
- Angular Aria
- Angular Material
- Angular Messages
- Node JS
- Typescript (1.8.x)
- Bower
- NPM
- Live Server
- Live Reload

## Typescript specifics

Typescript will be used for the following reasons:

- It is a superset of Javascipt
- Transpiles to ES5
- Use ES6+ features
- Static typing in development. Meaning you get compile-time errors instead of run-time errors

## Versioning

For transparency into our release cycle and in striving to maintain backward compatibility, this application is maintained under [the Semantic Versioning guidelines](http://semver.org/).